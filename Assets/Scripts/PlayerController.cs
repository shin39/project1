﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 5f;
    public float mouseSensitivity = 5f;
    public float rollSpeed = 2.5f;
    private int terrainHeight = 500;
    private int terrainWidth = 500;

    // Update is called once per frame
    void Update()
    {
        // horizontal movement
        Vector3 xMove = transform.right * Input.GetAxis("Horizontal");
        // vertical movement
        Vector3 yMove = transform.forward * Input.GetAxis("Vertical");

        // normalize vector and calculate velocity
        Vector3 velocity = (xMove+yMove).normalized * speed;
        Vector3 temp = gameObject.transform.localPosition + (velocity * Time.deltaTime);

        // stop movement when the player is trying to go out of boundary
        if (temp.z <= 0 || temp.z >= terrainHeight)
        {
            velocity.z = 0;
        }
        if(temp.x <= 0 || temp.x >= terrainWidth)
        {
            velocity.x = 0;
        }
        gameObject.transform.localPosition += (velocity * Time.deltaTime);

        // apply camera roll
        if (Input.GetKey(KeyCode.Q))
        {
            gameObject.transform.Rotate(new Vector3(0f, 0f, rollSpeed));
        }
        if (Input.GetKey(KeyCode.E))
        {
            gameObject.transform.Rotate(new Vector3(0f, 0f, -rollSpeed));
        }

        // calculate mouse movement as turning around
        float yRotation = Input.GetAxisRaw("Mouse X");
        float xRotation = Input.GetAxisRaw("Mouse Y");
        Vector3 rotation = new Vector3(-xRotation, yRotation, 0f) * mouseSensitivity;
        gameObject.transform.Rotate(rotation);

        
    }
}