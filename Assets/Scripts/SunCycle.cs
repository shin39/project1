﻿using UnityEngine;
using System.Collections;

public class SunCycle : MonoBehaviour {

    // keep track of time in seconds
    float time = 0;

    // the cycle to sunset
    public float dayLenght = 60;

    public Transform directionalLight;
    public GameObject sun;
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;

        // find transformation of sun based on time
        float degree = (time / dayLenght) * 180;

        directionalLight.rotation = Quaternion.Euler(new Vector3(degree, 0, 0));
        time = time % (dayLenght * 2);
    }
}
