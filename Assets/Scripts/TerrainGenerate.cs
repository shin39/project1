﻿using UnityEngine;
using System.Collections;

public class TerrainGenerate : MonoBehaviour {

    public Terrain terrain;
    private float maxY;
    private float maxX;

    void Start()
    {

        // set the maximum index for height map
        maxY = terrain.terrainData.heightmapHeight;
        maxX = terrain.terrainData.heightmapWidth;

        // procedurally generate and set the heightmap of the terrain
        SetHeightMap(terrain);

        // procedurally apply texture to the terrain based on the height of each pixel
        SetTerrainTexture(terrain.terrainData);
    }

    void SetHeightMap(Terrain terrain)
    {
        float seed = 0.2f;
        float[,] heightMap = new float[terrain.terrainData.heightmapWidth, terrain.terrainData.heightmapHeight];
        // set the terrain to be a flat terrain
        terrain.terrainData.SetHeights(0, 0, heightMap);

        // set the seed value to the corners
        heightMap[0, 0] = seed;
        heightMap[terrain.terrainData.heightmapWidth - 1, 0] = seed;
        heightMap[0, terrain.terrainData.heightmapHeight - 1] = seed;
        heightMap[terrain.terrainData.heightmapWidth - 1, terrain.terrainData.heightmapHeight - 1] = seed;

        // generate heightmap value with diamond square algorithm
        heightMap = DiamondSquare(heightMap, terrain.terrainData.heightmapWidth - 1, 0);
        terrain.terrainData.SetHeights(0, 0, heightMap);
    }

    /* procedural texture assignment
     * implemented based on the reference code from 
     * http://answers.unity3d.com/questions/12835/how-to-automatically-apply-different-textures-on-t.html 
     */
    void SetTerrainTexture(TerrainData terrainData)
    {
        float snowThreshold = 175f;
        float rockThreshold = 85f;
        // alphamap / splatmap data represents the weight of each texture 
        float[,,] alphamap = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float height = terrainData.GetHeight(y, x);
                // use Vector3 to represent 3 textures in the terrain
                Vector3 textureWeight = new Vector3(1, 0, 0);
                if (height > snowThreshold)
                {
                    // apply snowy texture 
                    textureWeight = Vector3.Lerp(textureWeight, new Vector3(0, 0, 1), (height - 0.5f) * 2);
                }
                else if (height < rockThreshold)
                {
                    // apply grass texture
                    textureWeight = Vector3.Lerp(textureWeight, new Vector3(0, 1, 0), height * 2);
                }
                // assign texture weight to each location in array
                textureWeight.Normalize();
                alphamap[x, y, 0] = textureWeight.x;
                alphamap[x, y, 1] = textureWeight.y;
                alphamap[x, y, 2] = textureWeight.z;
            }

        }
        terrainData.SetAlphamaps(0, 0, alphamap);
    }

    /* diamond square algorithm
     * implemented based on the reference code from 
     * http://www.playfuljs.com/realistic-terrain-in-130-lines/ 
     */
    float[,] DiamondSquare(float[,] map, int size, int iteration)
    {
        int x, y, half;
        half = size / 2;

        if (half < 1)
        {
            return map;
        }

        // perform diamond step
        for (y = half; y < maxY; y += size)
        {
            for (x = half; x < maxX; x += size)
            {
                map = DiamondStep(map, x, y, half, RandomHeight(iteration));
            }
        }

        // perform square step
        for (y = 0; y < maxY; y += half)
        {
            for (x = (y + half) % size; x < maxX; x += size)
            {
                map = SquareStep(map, x, y, half, RandomHeight(iteration));
            }
        }
        map = DiamondSquare(map, size / 2, iteration + 1);
        return map;
    }

    float CalcAverage(float i, float j, float k, float l)
    {
        return (i + j + k + l) * 0.25f;
    }

    float CalcAverage(float i, float j, float k)
    {
        return (i + j + k) * 0.33f;
    }

    float RandomHeight(int iteration)
    {
        // decrease random number every iteration by factor of 2
        return UnityEngine.Random.Range(-0.4f, 0.4f) / Mathf.Pow(2, iteration);
    }

    private float[,] DiamondStep(float[,] map, int x, int y, int half, float randNum)
    {
        // for each square in the array, set the midpoint of that square to be the
        // average of the four corner plus a random value
        float topLeft = map[x - half, y - half];
        float topRight = map[x - half, y + half];
        float bottomLeft = map[x + half, y - half];
        float bottomRight = map[x + half, y + half];

        if (topLeft == 0 || topRight == 0 || bottomLeft == 0 || bottomRight == 0)
            Debug.Log("error: one or more corner seed value is not set");

        map[x, y] = CalcAverage(topLeft, topRight, bottomLeft, bottomRight) + randNum;
        return map;
    }

    private float[,] SquareStep(float[,] map, int x, int y, int half, float randNum)
    {
        // for each diamond in the array, set the midpoint of that diamond to be the
        // average of the four corner points plus a random value

        float left = 0;
        float right = 0;
        float top = 0;
        float bottom = 0;

        if (y - half >= 0)
        {
            left = map[x, y - half];
        }
        if (y + half < maxY)
        {
            right = map[x, y + half];
        }
        if (x - half >= 0)
        {
            top = map[x - half, y];
        }
        if (x + half < maxX)
        {
            bottom = map[x + half, y];
        }

        map[x, y] = CalcAverage(top, left, right, bottom) + randNum;
        return map; ;
    }


}

