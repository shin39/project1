COMP30019 - Project 1
Erwin Haryantho <664480>


This program generates a 3D terrain in Unity and enable the user to navigate around the 
landscape in a flight simulator's style

This prgoram generates the landscape by making heightmap based on the
implementation of the diamond square algorithm. The algorithm begins 
by setting the four corner points of the map with a seed value and
then perform diamond step and square step recursively to set all the
points in the heightmap. There are also other features such as collision
between player and landscape and sun object to represent the illumination
and the time in the game world. The colour of the terrain corresponds
to the height of the terrain at any particular point (e.g. rocky surface
on normal terrain, snow on top of mountain and grass in valleys). No shadings
were implemented in this program because of the complex diffuse and specular
computation of terrain as opposed to a mesh object.


References
Diamond Square Algorithm :
- https://en.wikipedia.org/wiki/Diamond-square_algorithm
- http://www.playfuljs.com/realistic-terrain-in-130-lines/

Procedural Texture
-http://answers.unity3d.com/questions/12835/how-to-automatically-apply-different-textures-on-t.html
-https://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/
